package client;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

import rmi.EchoInt;

public class Echo {
  private static EchoInt eo;
  private static String myURL= "localhostdemo";


  
  public static void main(String[] args) throws UnknownHostException {
   /* if (args.length<2) {
        System.out.println("Usage: Echo <host> <port#>");
        System.exit(1);
    }*/
    
    /*for(int i=0;i<args.length;i++)
        System.out.println(args[i]);*/

    try {
          myURL=InetAddress.getLocalHost().getHostName();
     } catch (UnknownHostException e) {
          myURL="localhost";
     }
    
      System.out.println(myURL);
           

    //EJERCICIO: crear una instancia del stub
    eo =  new EchoObjectStub();

	  
    BufferedReader stdIn = new BufferedReader( new InputStreamReader(System.in));
    PrintWriter stdOut = new PrintWriter(System.out);
    String input,output,fin;
    
    try {
    	//Bucle que lee de teclado, invoca el eco y escribe respuesta en la pantalla:
    	input="";
    	fin="fin";
    	while(!input.equals(fin)) {
    		stdOut.println("Escriba cadena para invocar su eco...");
    		stdOut.flush();
        	input = stdIn.readLine(); //Lee cadena introducida por teclado
			//EJERCICIO: Invocar para la cadena leida el método echo del stub
        	output = eo.echo(input + " desde "+  myURL);
        	stdOut.println(output); //Escribe la respuesta del eco en la pantalla
    		stdOut.flush();
        }  	
    } catch (IOException e) {
    	System.err.println("I/O failed for connection to host: "+args[0]);
    }
  }
}